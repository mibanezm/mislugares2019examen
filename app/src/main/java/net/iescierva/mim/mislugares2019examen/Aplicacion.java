package net.iescierva.mim.mislugares2019examen;

import android.app.Application;

import net.iescierva.mim.mislugares2019examen.datos.LugaresLista;
import net.iescierva.mim.mislugares2019examen.datos.RepositorioLugares;
import net.iescierva.mim.mislugares2019examen.datos.AdaptadorLugares;
import net.iescierva.mim.mislugares2019examen.modelo.GeoException;
import net.iescierva.mim.mislugares2019examen.modelo.GeoPunto;

public class Aplicacion extends Application {

    public RepositorioLugares lugares;
    public AdaptadorLugares adaptador;

    @Override
    public void onCreate() {
        super.onCreate();
        lugares = new LugaresLista();
        adaptador = new AdaptadorLugares(lugares);
    }

    public RepositorioLugares getLugares() {
        return lugares;
    }
}
