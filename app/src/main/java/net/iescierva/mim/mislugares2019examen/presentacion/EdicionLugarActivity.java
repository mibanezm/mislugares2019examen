package net.iescierva.mim.mislugares2019examen.presentacion;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import net.iescierva.mim.mislugares2019examen.Aplicacion;
import net.iescierva.mim.mislugares2019examen.R;
import net.iescierva.mim.mislugares2019examen.casos_uso.CasosUsoLugar;
import net.iescierva.mim.mislugares2019examen.datos.RepositorioLugares;
import net.iescierva.mim.mislugares2019examen.modelo.Lugar;
import net.iescierva.mim.mislugares2019examen.modelo.TipoLugar;

import androidx.appcompat.app.AppCompatActivity;

public class EdicionLugarActivity extends AppCompatActivity {

    private RepositorioLugares lugares;
    private CasosUsoLugar usoLugar;
    private int pos;
    private Lugar lugar;
    private EditText nombre;
    private Spinner tipo;
    private EditText direccion;
    private EditText codigoPostal;
    private EditText telefono;
    private EditText url;
    private EditText comentario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edicion_lugar);
        getExtras();
        createSpinner();
        update();
    }

    private void getExtras() {
        Bundle extras = getIntent().getExtras();
        assert extras != null;

        pos = extras.getInt("pos", 0);
        lugares = ((Aplicacion) getApplication()).lugares;
        usoLugar = new CasosUsoLugar(this, lugares);
        lugar = lugares.elemento(pos);
    }

    private void createSpinner() {
        tipo = findViewById(R.id.spinner);

        ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, TipoLugar.getNombres());

        adaptador.setDropDownViewResource(android.R.layout.
                simple_spinner_dropdown_item);

        tipo.setAdapter(adaptador);
        tipo.setSelection(lugar.getTipo().ordinal());
    }

    private void update() {
        nombre = findViewById(R.id.editTextNombre);
        nombre.setText(lugar.getNombre());

        direccion = findViewById(R.id.editTextDireccion);
        direccion.setText(lugar.getDireccion());

        codigoPostal=findViewById(R.id.editTextCodigoPostal);
        codigoPostal.setText(lugar.getCodigoPostal());

        telefono = findViewById(R.id.editTextTelefono);
        telefono.setText(String.valueOf(lugar.getTelefono()));

        url = findViewById(R.id.editTextDireccionWeb);
        url.setText(lugar.getUrl());

        comentario = findViewById(R.id.editTextComentario);
        comentario.setText(lugar.getComentario());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edicion_lugar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.accion_cancelar:
                finish();
                return true;
            case R.id.accion_guardar:
                lugar.setNombre(nombre.getText().toString());
                lugar.setTipo(TipoLugar.values()[tipo.getSelectedItemPosition()]);
                lugar.setDireccion(direccion.getText().toString());
                lugar.setCodigoPostal(codigoPostal.getText().toString());
                lugar.setTelefono(Integer.parseInt(telefono.getText().toString()));
                lugar.setUrl(url.getText().toString());
                lugar.setComentario(comentario.getText().toString());
                usoLugar.guardar(pos, lugar);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}