package net.iescierva.mim.mislugares2019examen.presentacion;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import net.iescierva.mim.mislugares2019examen.Aplicacion;
import net.iescierva.mim.mislugares2019examen.R;
import net.iescierva.mim.mislugares2019examen.casos_uso.CasosUsoActividades;
import net.iescierva.mim.mislugares2019examen.datos.AdaptadorLugares;
import net.iescierva.mim.mislugares2019examen.datos.RepositorioLugares;
import net.iescierva.mim.mislugares2019examen.casos_uso.CasosUsoLugar;
import net.iescierva.mim.mislugares2019examen.modelo.GeoException;
import net.iescierva.mim.mislugares2019examen.modelo.TierraMarPunto;

public class MainActivity extends AppCompatActivity {

    //Common info
    private static RepositorioLugares lugares;
    private static CasosUsoLugar usoLugar;
    private static CasosUsoActividades usoActividades;

    //RecyclerView elements
    private RecyclerView recyclerView;
    public AdaptadorLugares adaptador;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.ItemDecoration separador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        //inicialización usando Aplicacion
        lugares = ((Aplicacion) getApplication()).lugares;
        usoLugar = new CasosUsoLugar(this, lugares);
        usoActividades = new CasosUsoActividades(this, lugares);

        //acceder a la vista "recycler_view" de la actividad actual (MainActivity)
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        //Asignarle el adaptador
        adaptador = new AdaptadorLugares(this, lugares);
        recyclerView.setAdapter(adaptador);

        //Asignarle el LayoutManager, en este caso de tipo Linear
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        //asignarle el separador de elementos
        separador = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(separador);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            //lanzar "Preferencias" mediante el menú
            lanzarPreferencias(null);
            return true;
        }
        if (id == R.id.acercaDe) {
            //lanzar "Acerca de" mediante el menú
            lanzarAcercaDe(null);
            return true;
        }
        if (id == R.id.menu_buscar) {
            //lanzar "Vista Lugar" mediante el menú
            lanzarVistaLugar(null);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void lanzarPreferencias(View view) {
        Intent i = new Intent(this, PreferenciasActivity.class);
        startActivity(i);
    }

    public void lanzarAcercaDe(View view) {
        Intent i = new Intent(this, AcercaDeActivity.class);
        startActivity(i);
    }

    public void lanzarVistaLugar(View view) {
        final EditText entrada = new EditText(this);
        entrada.setText("0");
        new AlertDialog.Builder(this)
                .setTitle("Selección de lugar")
                .setMessage("indica su id:")
                .setView(entrada)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        int id = Integer.parseInt(entrada.getText().toString());
                        usoLugar.mostrar(id);
                    }
                })
                .setNegativeButton("Cancelar", null)
                .show();
    }


    public void mostrarPreferencias(View view) {
        SharedPreferences pref =
                PreferenceManager.getDefaultSharedPreferences(this);
        String s = "notificaciones: " + pref.getBoolean("notificaciones", true)
                + ", máximo a listar: " + pref.getString("maximo", "?")
                + ", orden: " + pref.getString("orden", "1")
                + ", recibir correos: " + pref.getBoolean("correos", false)
                + ", correo: " + pref.getString("correo", "?")
                + ", tipos: " + pref.getString("tipos", "?");
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    public void salir(View view) {
        finish();
    }

}
