package net.iescierva.mim.mislugares2019examen.casos_uso;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;

import net.iescierva.mim.mislugares2019examen.R;
import net.iescierva.mim.mislugares2019examen.datos.RepositorioLugares;
import net.iescierva.mim.mislugares2019examen.modelo.Lugar;
import net.iescierva.mim.mislugares2019examen.presentacion.EdicionLugarActivity;
import net.iescierva.mim.mislugares2019examen.presentacion.VistaLugarActivity;

public class CasosUsoLugar {
    private Activity actividad;
    private RepositorioLugares lugares;

    /**
     * @param actividad
     * @param lugares
     */
    public CasosUsoLugar(Activity actividad, RepositorioLugares lugares) {
        this.actividad = actividad;
        this.lugares = lugares;
    }

    /**
     * @param pos
     */
    // OPERACIONES BÁSICAS
    public void mostrar(int pos) {
        Intent i = new Intent(actividad, VistaLugarActivity.class);
        i.putExtra("pos", pos);
        actividad.startActivity(i);
    }

    public void borrar(final int id) {
        new AlertDialog.Builder(actividad)
                .setTitle(R.string.dialogo_borrar_titulo)
                .setMessage(R.string.dialogo_borrar_texto)

                .setPositiveButton(R.string.dialogo_borrar_aceptar, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        lugares.delete(id);
                        actividad.finish();
                    }})
                .setNegativeButton(R.string.dialogo_borrar_cancelar, null)
                .setCancelable(false)
                .show();
    }

    public void guardar(int id, Lugar nuevoLugar) {
        lugares.update(id, nuevoLugar);
    }

    public void editar(int pos, int codigoSolicitud) {
        Intent i = new Intent(actividad, EdicionLugarActivity.class);
        i.putExtra("pos", pos);
        actividad.startActivityForResult(i, codigoSolicitud);
    }
}