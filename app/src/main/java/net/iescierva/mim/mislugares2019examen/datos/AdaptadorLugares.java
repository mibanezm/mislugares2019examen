/*
 * Copyright (c) 2019. Miguel Angel
 */

package net.iescierva.mim.mislugares2019examen.datos;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import net.iescierva.mim.mislugares2019examen.R;
import net.iescierva.mim.mislugares2019examen.modelo.GeoException;
import net.iescierva.mim.mislugares2019examen.modelo.GeoPunto;
import net.iescierva.mim.mislugares2019examen.modelo.Lugar;
import net.iescierva.mim.mislugares2019examen.Aplicacion;

import androidx.recyclerview.widget.RecyclerView;

public class AdaptadorLugares extends RecyclerView.Adapter<AdaptadorLugares.ViewHolder> {
    protected RepositorioLugares lugares;         // Lista de lugares a mostrar
    protected LayoutInflater inflador;   //Crea Layouts a partir del XML
    protected Context contexto;          //Lo necesitamos para el inflador


    public AdaptadorLugares(RepositorioLugares lugares) {
        this.lugares = lugares;
    }

    public AdaptadorLugares(Context contexto, RepositorioLugares lugares) {
        this.contexto = contexto;
        this.lugares = lugares;
        inflador = (LayoutInflater) contexto
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    //Creamos nuestro ViewHolder, con los tipos de elementos a modificar
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView nombre, direccion,codigoPostal,distancia;
        public ImageView foto;
        public RatingBar valoracion;        //inicialización usando Aplicacion

        public ViewHolder(View itemView) {
            super(itemView);
            nombre = itemView.findViewById(R.id.nombre);
            direccion = itemView.findViewById(R.id.direccion);
            codigoPostal=itemView.findViewById(R.id.codigoPostal);
            foto = itemView.findViewById(R.id.foto);
            valoracion = itemView.findViewById(R.id.valoracion);
            distancia=itemView.findViewById(R.id.distancia);
        }

        // Personalizamos un ViewHolder a partir de un lugar
        public void personaliza(Lugar lugar) {
            GeoPunto ies=new GeoPunto();
            nombre.setText(lugar.getNombre());
            direccion.setText(lugar.getDireccion());
            codigoPostal.setText("C.P. "+lugar.getCodigoPostal());
            try {
                ies = new GeoPunto(37.9611805, -1.1335914);
            } catch (GeoException e) {};
            distancia.setText(String.format("%.2f", ies.distancia(lugar.getPosicion())/1000)+"km");
            int id = R.drawable.otros;
            switch (lugar.getTipo()) {
                case RESTAURANTE:
                    id = R.drawable.restaurante;
                    break;
                case BAR:
                    id = R.drawable.bar;
                    break;
                case COPAS:
                    id = R.drawable.copas;
                    break;
                case ESPECTACULO:
                    id = R.drawable.espectaculos;
                    break;
                case HOTEL:
                    id = R.drawable.hotel;
                    break;
                case COMPRAS:
                    id = R.drawable.compras;
                    break;
                case EDUCACION:
                    id = R.drawable.educacion;
                    break;
                case DEPORTE:
                    id = R.drawable.deporte;
                    break;
                case NATURALEZA:
                    id = R.drawable.naturaleza;
                    break;
                case GASOLINERA:
                    id = R.drawable.gasolinera;
                    break;
                case SITIO_HISTORICO:
                    id = R.drawable.sitio_historico;
                    break;

            }
            foto.setImageResource(id);
            foto.setScaleType(ImageView.ScaleType.FIT_END);
            valoracion.setRating(lugar.getValoracion());
        }
    }

    // Creamos el ViewHolder con la vista de un elemento sin personalizar
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Inflar la Vista
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.elemento_lista, parent, false);
        return new ViewHolder(v);
    }

    // Vuando se activa el ViewHolder lo personalizamos
    @Override
    public void onBindViewHolder(ViewHolder holder, int posicion) {
        Lugar lugar = lugares.elemento(posicion);
        holder.personaliza(lugar);
    }

    // Número de elementos
    @Override
    public int getItemCount() {
        return lugares.size();
    }
}
