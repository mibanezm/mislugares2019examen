/*
 * Copyright (c) 2019. Miguel Angel
 */

package net.iescierva.mim.mislugares2019examen.modelo;

public class GeoException extends Exception {
    GeoException(String e) {
        super(e);
    }
}
