/*
 * Copyright (c) 2019. Miguel Angel
 */

package net.iescierva.mim.mislugares2019examen.modelo;

public class TierraMarPunto extends GeoPuntoAlt {

    public final static double PROFUNDIDA_MAXIMA = -11000;   // Fosa de las Marianas
    public final static double PROFUNDIDAD_MINIMA = 0;   // Nivel del mar

    private double profundidad;

    public TierraMarPunto(double latitud, double longitud, double altitud) throws GeoException {
        super(latitud,longitud);
        if (altitud>=0)
            setAltitud(altitud);
        else
            setProfundidad(altitud);
    }

    public void setProfundidad(double profundidad) throws GeoException {
        if (isProfundidadOk(profundidad))
            this.profundidad=profundidad;
        else
            throw new GeoException("TierraMarPunto.setProfundidad(double): invalid profundidad");
    }
    public boolean isProfundidadOk() {
            return PROFUNDIDA_MAXIMA<= getAltitud() && getAltitud()<=PROFUNDIDAD_MINIMA;
    }

    public static boolean isProfundidadOk(double profundidad) {
        return PROFUNDIDA_MAXIMA<=profundidad && profundidad<=PROFUNDIDAD_MINIMA;
    }

    @Override
    public boolean isAltitudOk() {
        if (getAltitud()>=0)
            return super.isAltitudOk();
        else
            return isProfundidadOk();
    }

    @Override
    public String toString() {
        return "TierraMarPunto{"+super.toString()+","+profundidad+"}";
    }
}
