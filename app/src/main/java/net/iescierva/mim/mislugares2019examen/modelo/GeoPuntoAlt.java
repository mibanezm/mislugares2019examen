/*
 * Copyright (c) 2019. Miguel Angel
 */

package net.iescierva.mim.mislugares2019examen.modelo;

public class GeoPuntoAlt extends GeoPunto {
    private double altitud;

    public final static double ALTITUD_MAXIMA = 9000;   // >Everest
    public final static double ALTITUD_MINIMA = -500;   // <Mar Muerto

    public GeoPuntoAlt(double latitud, double longitud, double altitud) throws GeoException {
        super(latitud, longitud);
        this.altitud = altitud;
    }

    public GeoPuntoAlt(double latitud, double longitud) throws GeoException {
        this(latitud, longitud,0);
    }

    public double distancia(GeoPuntoAlt punto) {
        double d = super.distancia(punto);
        return Math.sqrt(Math.pow(d, 2) + Math.pow(punto.altitud - this.altitud, 2));
    }

    public double getAltitud() {
        return altitud;
    }

    public void setAltitud(double altitud) throws GeoException {
        if (altitud >= ALTITUD_MINIMA && altitud <= ALTITUD_MAXIMA)
            this.altitud = altitud;
        else
            throw new GeoException("GeoPuntoAlt.setAltitud(double): {" + altitud + "}");
    }

    public boolean isAltitudOk() {
        return altitud >= ALTITUD_MINIMA && altitud <= ALTITUD_MAXIMA;
    }

    public static boolean isAltitudOk(double altitud) {
        return altitud >= ALTITUD_MINIMA && altitud <= ALTITUD_MAXIMA;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GeoPuntoAlt)) return false;
        if (!super.equals(o)) return false;
        GeoPuntoAlt that = (GeoPuntoAlt) o;
        return Double.compare(that.altitud, altitud) == 0;
    }

    /*
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), altitud);
    }
    */

    @Override
    public String toString() {
        return "GeoPuntoAlt{" + super.toString() +
                ",altitud=" + altitud +
                '}';
    }
}
