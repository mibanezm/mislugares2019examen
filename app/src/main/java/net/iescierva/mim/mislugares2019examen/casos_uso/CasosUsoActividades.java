
/*
 * Copyright (c) 2019. Miguel Angel
 */

package net.iescierva.mim.mislugares2019examen.casos_uso;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.EditText;

import net.iescierva.mim.mislugares2019examen.R;
import net.iescierva.mim.mislugares2019examen.datos.RepositorioLugares;
import net.iescierva.mim.mislugares2019examen.presentacion.AcercaDeActivity;
import net.iescierva.mim.mislugares2019examen.presentacion.PreferenciasActivity;

public class CasosUsoActividades {

    private Activity actividad;
    private CasosUsoLugar usoLugar;
    private RepositorioLugares lugares;

    public CasosUsoActividades(Activity actividad, RepositorioLugares lugares) {
        this.actividad = actividad;
        this.lugares = lugares;
        usoLugar = new CasosUsoLugar(actividad, lugares);
    }

    public void lanzarAcercaDe(Activity activity) {
        Intent i = new Intent(activity, AcercaDeActivity.class);
        activity.startActivity(i);
    }

    public void lanzarPreferencias(Activity activity) {
        Intent i = new Intent(activity, PreferenciasActivity.class);
        activity.startActivity(i);
    }

    public void lanzarVistaLugarActividad(View view) {
        final EditText entrada = new EditText(actividad);
        entrada.setText(R.string.dialogo_buscar_default_value);
        new AlertDialog.Builder(actividad)
                .setTitle(R.string.titulo_modal_buscar)
                .setMessage(R.string.texto_modal_buscar)
                .setView(entrada)
                .setPositiveButton(R.string.dialogo_borrar_aceptar, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        int id = Integer.parseInt (entrada.getText().toString());
                        usoLugar.mostrar(id);
                    }})
                .setNegativeButton(R.string.dialogo_borrar_cancelar, null)
                .show();
    }
}
